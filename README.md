# Vue.js CRUD App with Vue Router & Axios

For more detail, please visit:

> [Vue.js CRUD App with Vue Router & Axios](https://github.com/thiagoigpereira/vue-client-crud/)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your tests

```
yarn lint
```
